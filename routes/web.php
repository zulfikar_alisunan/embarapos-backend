<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth
Route::get('login', [AuthController::class, 'login']);
Route::post('login', [AuthController::class, 'loginPost']);
Route::get('logout', [AuthController::class, 'logOut']);

Route::group(['middleware' => 'check-login'], function(){
    Route::get('/', [IndexController::class, 'index']);
});
