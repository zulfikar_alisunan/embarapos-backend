@extends('backend.auth.master')
@section('content')
<div class="page-content">
    <div class="container text-dark">
        <div class="row">
            <div class="col-lg-4 d-block mx-auto">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="text-center mb-6">
                                    <img src="../../assets/images/brand/logo.png" class="header-brand-img main-logo" alt="IndoUi logo">
                                    <img src="../../assets/images/brand/logo-light.png" class="header-brand-img dark-main-logo" alt="IndoUi logo">
                                </div>
                                <h3 class="text-center">Login</h3>
                                <p class="text-muted text-center">Sign In to your account</p>
                                @if (Session::has('status'))
                                    <div class="alert alert-{{ Session::get('alert-class') }}" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <i class="fa fa-{{ Session::get('icon') }} mr-2" aria-hidden="true"></i> {{ Session::get('status') }}
                                    </div>
                                @endif
                                <form action="" method="POST">
                                    @csrf
                                    <label for="email_or_user_name">Email or user name</label>
                                    <div class="input-group mb-3">
                                        <span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
                                        <input type="text" name="email_or_user_name" class="form-control" placeholder="Username">
                                    </div>
                                    <label for="email_or_user_name">Password</label>
                                    <div class="input-group mb-4">
                                        <span class="input-group-addon bg-white"><i class="fa fa-unlock-alt"></i></span>
                                        <input type="password" name="password" class="form-control" placeholder="Password">
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary btn-block">Login</button>
                                        </div>
                                        <div class="col-12">
                                            <a href="#" class="btn btn-link box-shadow-0 px-0">Forgot password?</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection