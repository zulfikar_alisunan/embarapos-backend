<!--Header-->
<div class="hor-header header d-flex navbar-collapse">
    <div class="container">
        <div class="d-flex">
            <a class="animated-arrow hor-toggle horizontal-navtoggle"><span></span></a>
            <a class="header-brand" href="index.html">
                <img src="../../assets/images/brand/logo.png" class="header-brand-img main-logo" alt="IndoUi logo">
                <img src="../../assets/images/brand/logo-light.png" class="header-brand-img dark-main-logo" alt="IndoUi logo">
                <img src="../../assets/images/brand/icon-light.png" class="header-brand-img dark-icon-logo" alt="IndoUi logo">
                <img src="../../assets/images/brand/icon.png" class="header-brand-img icon-logo" alt="IndoUi logo">
            </a><!-- logo-->
            <div class="d-flex order-lg-2 ml-auto header-right">
                <div class="d-md-flex header-search" id="bs-example-navbar-collapse-1">
                    <form class="navbar-form" role="search">
                        <div class="input-group ">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="reset" class="btn btn-default">
                                    <i class="fe fe-x"></i>
                                </button>
                                {{-- <button type="submit" class="btn btn-default">
                                    <i class="fe fe-search"></i>
                                </button> --}}
                            </span>
                        </div>
                    </form>
                </div><!-- Search -->
                <div class="d-md-flex">
                    <a href="#" class="nav-link icon full-screen-link">
                        <i class="fe fe-minimize fullscreen-button"></i>
                    </a>
                </div>
                {{-- <div class="dropdown d-md-flex header-message">
                    <a class="nav-link icon" data-toggle="dropdown">
                        <i class="fe fe-bell"></i>
                        <span class="nav-unread badge badge-danger badge-pill">3</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <a class="dropdown-item text-center" href="#">Notifications</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item d-flex pb-4" href="#">
                            <span class="avatar mr-3 br-3 align-self-center avatar-md cover-image bg-primary-transparent text-primary"><i class="fe fe-mail"></i></span>
                            <div>
                                <span class="font-weight-bold"> Commented on your post </span>
                                <div class="small text-muted d-flex">
                                    3 hours ago
                                </div>
                            </div>
                        </a>
                        <a class="dropdown-item d-flex pb-4" href="#">
                            <span class="avatar avatar-md br-3 mr-3 align-self-center cover-image bg-secondary-transparent text-secondary"><i class="fe fe-download"></i>
                            </span>
                            <div>
                                <span class="font-weight-bold"> New file has been Uploaded </span>
                                <div class="small text-muted d-flex">
                                    5 hour ago
                                </div>
                            </div>
                        </a>
                        <a class="dropdown-item d-flex pb-4" href="#">
                            <span class="avatar avatar-md br-3 mr-3 align-self-center cover-image bg-warning-transparent text-warning"><i class="fe fe-user"></i>
                            </span>
                            <div>
                                <span class="font-weight-bold"> User account has Updated</span>
                                <div class="small text-muted d-flex">
                                    20 mins ago
                                </div>
                            </div>
                        </a>
                        <a class="dropdown-item d-flex pb-4" href="#">
                            <span class="avatar avatar-md br-3 mr-3 align-self-center cover-image bg-info-transparent text-info"><i class="fe fe-shopping-cart"></i>
                            </span>
                            <div>
                                <span class="font-weight-bold"> New Order Recevied</span>
                                <div class="small text-muted d-flex">
                                    1 hour ago

                                </div>
                            </div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <div class="text-center dropdown-btn pb-3">
                            <div class="btn-list">
                                <a href="#" class="btn btn-primary btn-sm"><i class="fe fe-plus mr-1"></i>Add New</a>
                                <a href="#" class=" btn btn-secondary btn-sm"><i class="fe fe-eye mr-1"></i>View All</a>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <!--Navbar -->
                <div class="dropdown header-profile">
                    <a class="nav-link pr-0 leading-none d-flex pt-1" data-toggle="dropdown" href="#">
                        <span class="avatar avatar-md brround cover-image" data-image-src="{{ asset('assets/images/users/female/2.jpg') }}"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <div class="drop-heading">
                            <div class="text-center">
                                <h5 class="text-dark mb-1">{{ Auth::user()->name }}</h5>
                                <small class="text-muted">{{ Auth::user()->role }}</small>
                            </div>
                        </div>
                        <div class="dropdown-divider m-0"></div>
                        {{-- <a class="dropdown-item" href="#"><i class="dropdown-icon fe fe-user"></i>Profile</a>
                        <a class="dropdown-item" href="#"><i class="dropdown-icon fe fe-edit"></i>Schedule</a>
                        <a class="dropdown-item" href="#"><i class="dropdown-icon fe fe-mail"></i> Inbox</a>
                        <a class="dropdown-item" href="#"><i class="dropdown-icon fe fe-unlock"></i> Look Screen</a> --}}
                        <a class="dropdown-item" href="{{ url('logout') }}"><i class="dropdown-icon fe fe-power"></i> Log Out</a>
                        <div class="dropdown-divider"></div>
                        {{-- <div class="text-center dropdown-btn pb-3">
                            <div class="btn-list">
                                <a href="#" class="btn btn-icon btn-facebook btn-sm"><i class="icon icon-social-facebook"></i></a>
                                <a href="#" class="btn btn-icon btn-twitter btn-sm"><i class="icon icon-social-twitter"></i></a>
                                <a href="#" class="btn btn-icon btn-instagram btn-sm"><i class="icon icon-social-instagram"></i></a>
                            </div>
                        </div> --}}
                    </div>
                </div>
                {{-- <div class="dropdown d-md-flex Sidebar-setting">
                    <a href="#" class="nav-link icon" data-toggle="sidebar-right" data-target=".sidebar-right">
                        <i class="fe fe-more-horizontal"></i>
                    </a>
                </div> --}}
            </div>
        </div>
    </div>
</div>
<!--Header end-->

<!--Horizontal-main -->
<div class="sticky">
    <div class="horizontal-main hor-menu clearfix">
        <div class="horizontal-mainwrapper container clearfix">
            <!--Nav-->
            <nav class="horizontalMenu clearfix">
                <ul class="horizontalMenu-list">
                    <li aria-haspopup="true"><a href="#" class="sub-icon @yield('dashboard')"><i class="fe fe-airplay"></i> Dashboard <i class="fa fa-angle-down horizontal-icon"></i></a>
                        <ul class="sub-menu">
                            <li aria-haspopup="true"><a href="index.html">Analytics Dashboard</a></li>
                            <li aria-haspopup="true"><a href="index2.html">E-Commerce Dashboard</a></li>
                            <li aria-haspopup="true"><a href="index3.html">Sales Dashboard</a></li>
                            <li aria-haspopup="true"><a href="index4.html">IT Dashboard</a></li>
                            <li aria-haspopup="true"><a href="index5.html">Logistics Dashboard</a></li>
                        </ul>
                    </li>
                    <li aria-haspopup="true"><a href="widgets.html" class=""><i class="fe fe-codepen"></i> Widgets</a></li>
                    <li aria-haspopup="true"><a href="#" class="sub-icon"><i class="fe fe-bar-chart-2"></i> Charts <i class="fa fa-angle-down horizontal-icon"></i></a>
                        <ul class="sub-menu">
                            <li aria-haspopup="true"><a href="chart-chartist.html">Chartist Charts</a></li>
                            <li aria-haspopup="true"><a href="chart-morris.html">Morris Charts</a></li>
                            <li aria-haspopup="true"><a href="chart-js.html">Charts js</a></li>
                            <li aria-haspopup="true"><a href="chart-peity.html">Pie Charts</a></li>
                            <li aria-haspopup="true"><a href="chart-echart.html">Echart Charts</a></li>
                            <li aria-haspopup="true"><a href="chart-flot.html">Flot Charts</a></li>
                            <li aria-haspopup="true"><a href="chart-nvd3.html">Nvd3 Charts</a></li>
                            <li aria-haspopup="true"><a href="chart-dygraph.html">Dygraph Charts</a></li>
                        </ul>
                    </li>
                    <li aria-haspopup="true"><a href="#" class="sub-icon"><i class="fe fe-box"></i> Elements  <i class="fa fa-angle-down horizontal-icon"></i></a>
                        <div class="horizontal-megamenu clearfix">
                            <div class="container">
                                <div class="mega-menubg">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-12 col-xs-12 link-list">
                                            <ul>
                                                <li aria-haspopup="true"><a href="alerts.html">Alerts</a></li>
                                                <li aria-haspopup="true"><a href="buttons.html">Buttons</a></li>
                                                <li aria-haspopup="true"><a href="colors.html">Colors</a></li>
                                                <li aria-haspopup="true"><a href="cards.html">Cards design</a></li>
                                                <li aria-haspopup="true"><a href="cards-image.html">Image  Cards design</a></li>
                                                <li aria-haspopup="true"><a href="avatars.html">Avatars</a></li>
                                                <li aria-haspopup="true"><a href="dropdown.html">Dropdowns</a></li>
                                                <li aria-haspopup="true"><a href="thumbnails.html">Thumbnails</a></li>
                                                <li aria-haspopup="true"><a href="mediaobject.html">Media Object</a></li>
                                                <li aria-haspopup="true"><a href="list.html"> List</a></li>
                                                <li aria-haspopup="true"><a href="tags.html">Tags</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-3 col-md-12 col-xs-12 link-list">
                                            <ul>
                                                <li aria-haspopup="true"><a href="pagination.html">Pagination</a></li>
                                                <li aria-haspopup="true"><a href="navigation.html">Navigation</a></li>
                                                <li aria-haspopup="true"><a href="typography.html">Typography</a></li>
                                                <li aria-haspopup="true"><a href="breadcrumbs.html">Breadcrumbs</a></li>
                                                <li aria-haspopup="true"><a href="badge.html">Badges</a></li>
                                                <li aria-haspopup="true"><a href="jumbotron.html">Jumbotron</a></li>
                                                <li aria-haspopup="true"><a href="panels.html">Panels</a></li>
                                                <li aria-haspopup="true"><a href="modal.html">Modal</a></li>
                                                <li aria-haspopup="true"><a href="tooltipandpopover.html">Tooltip and popover</a></li>
                                                <li aria-haspopup="true"><a href="progress.html">Progress</a></li>
                                                <li aria-haspopup="true"><a href="carousel.html">Carousels</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-3 col-md-12 col-xs-12 link-list">
                                            <ul>
                                                <li aria-haspopup="true"><a href="maps.html">Maps</a></li>
                                                <li><h3 class="fs-14 font-weight-bold mb-1">Tables</h3></li>
                                                <li aria-haspopup="true"><a href="tables.html">Default table</a></li>
                                                <li aria-haspopup="true"><a href="datatable.html"> Data Tables</a></li>
                                                <li><h3 class="fs-14 font-weight-bold mb-1 mt-3">Forms</h3></li>
                                                <li aria-haspopup="true"><a href="form-elements.html">Form Elements</a></li>
                                                <li aria-haspopup="true"><a href="form-wizard.html">Form Wizard</a></li>
                                                <li aria-haspopup="true"><a href="wysiwyag.html">Form Editor</a></li>
                                                <li><h3 class="fs-14 font-weight-bold mb-1 mt-3">Calendar</h3></li>
                                                <li aria-haspopup="true"><a href="calendar.html">Default calendar</a></li>
                                                <li aria-haspopup="true"><a href="calendar2.html">Full calendar</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-3 col-md-12 col-xs-12 link-list">
                                            <ul>
                                                <li><h3 class="fs-14 font-weight-bold mb-3">Error Pages</h3></li>
                                                <li aria-haspopup="true"><a href="400.html">400 Error</a></li>
                                                <li aria-haspopup="true"><a href="401.html">401 Error</a></li>
                                                <li aria-haspopup="true"><a href="403.html">403 Error</a></li>
                                                <li aria-haspopup="true"><a href="404.html">404 Error</a></li>
                                                <li aria-haspopup="true"><a href="500.html">500 Error</a></li>
                                                <li aria-haspopup="true"><a href="503.html">503 Error</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li aria-haspopup="true"><a href="#" class="sub-icon"><i class="fe fe-layers"></i>Advanced UI <i class="fa fa-angle-down horizontal-icon"></i></a>
                        <ul class="sub-menu">
                            <li aria-haspopup="true"><a href="chat.html"> Default Chat</a></li>
                            <li aria-haspopup="true"><a href="notify.html">Notifications</a></li>
                            <li aria-haspopup="true"><a href="sweetalert.html">Sweet alerts</a></li>
                            <li aria-haspopup="true"><a href="rangeslider.html">Range slider</a></li>
                            <li aria-haspopup="true"><a href="scroll.html">Content Scroll bar</a></li>
                            <li aria-haspopup="true"><a href="counters.html">Counters</a></li>
                            <li aria-haspopup="true"><a href="loaders.html">Loaders</a></li>
                            <li aria-haspopup="true"><a href="time-line.html">Time Line</a></li>
                            <li aria-haspopup="true"><a href="rating.html">Rating</a></li>
                            <li aria-haspopup="true"><a href="accordion.html">Accordions</a></li>
                            <li aria-haspopup="true"><a href="tabs.html"> Tabs</a></li>
                            <li aria-haspopup="true"><a href="footers.html">Footers</a></li>
                            <li aria-haspopup="true"><a href="crypto-currencies.html">Crypto-currencies</a></li>
                            <li aria-haspopup="true"><a href="users-list.html">User List</a></li>
                            <li aria-haspopup="true"><a href="search.html">Search page</a></li>
                        </ul>
                    </li>
                    <li aria-haspopup="true"><a href="#" class="sub-icon"><i class="fe fe-compass"></i> Icons <i class="fa fa-angle-down horizontal-icon"></i></a>
                        <ul class="sub-menu">
                            <li aria-haspopup="true"><a href="icons.html">Font Awesome</a></li>
                            <li aria-haspopup="true"><a href="icons2.html">Material Design Icons</a></li>
                            <li aria-haspopup="true"><a href="icons3.html">Simple Line Icons</a></li>
                            <li aria-haspopup="true"><a href="icons4.html">Feather Icons</a></li>
                            <li aria-haspopup="true"><a href="icons5.html">Ionic Icons</a></li>
                            <li aria-haspopup="true"><a href="icons6.html">Flag Icons</a></li>
                            <li aria-haspopup="true"><a href="icons7.html">pe7 Icons</a></li>
                            <li aria-haspopup="true"><a href="icons8.html">Themify Icons</a></li>
                            <li aria-haspopup="true"><a href="icons9.html">Typicons Icons</a></li>
                            <li aria-haspopup="true"><a href="icons10.html">Weather Icons</a></li>
                        </ul>
                    </li>
                    <li aria-haspopup="true"><a href="#" class="sub-icon"><i class=" fe fe-briefcase"></i> Pages <i class="fa fa-angle-down horizontal-icon"></i></a>
                        <ul class="sub-menu">
                            <li aria-haspopup="true" class="sub-menu-sub"><a href="#">Profile</a>
                                <ul class="sub-menu">
                                    <li aria-haspopup="true"><a href="profile.html"> Profile </a></li>
                                    <li aria-haspopup="true"><a href="editprofile.html">Edit Profile</a></li>
                                </ul>
                            </li>
                            <li aria-haspopup="true" class="sub-menu-sub"><a href="#">Email</a>
                                <ul class="sub-menu">
                                    <li aria-haspopup="true"><a href="email.html">Email</a></li>
                                    <li aria-haspopup="true"><a href="emailservices.html">Email Inbox</a></li>
                                </ul>
                            </li>
                            <li aria-haspopup="true"><a href="gallery.html">Gallery</a></li>
                            <li aria-haspopup="true"><a href="about.html">About Company</a></li>
                            <li aria-haspopup="true"><a href="services.html">Services</a></li>
                            <li aria-haspopup="true"><a href="faq.html">FAQS</a></li>
                            <li aria-haspopup="true"><a href="terms.html">Terms and Conditions</a></li>
                            <li aria-haspopup="true"><a href="empty.html">Empty Page</a></li>
                            <li aria-haspopup="true"><a href="construction.html">Under Construction</a></li>
                            <li aria-haspopup="true"><a href="blog.html">Blog</a></li>
                            <li aria-haspopup="true"><a href="invoice.html">Invoice</a></li>
                            <li aria-haspopup="true" ><a href="pricing.html">Pricing Tables</a></li>
                        </ul>
                    </li>
                    <li aria-haspopup="true"><a href="#" class="sub-icon"><i class="fe fe-shopping-cart"></i>E-Commerce <i class="fa fa-angle-down horizontal-icon"></i></a>
                        <ul class="sub-menu">
                            <li aria-haspopup="true"><a href="product.html"> Products</a></li>
                            <li aria-haspopup="true"><a href="product-details.html">Product Details</a></li>
                            <li aria-haspopup="true"><a href="cart.html">Shopping Cart</a></li>
                        </ul>
                    </li>
                    <li aria-haspopup="true"><a href="#" class="sub-icon"><i class="fe fe-unlock"></i>Custom <i class="fa fa-angle-down horizontal-icon"></i></a>
                        <ul class="sub-menu">
                            <li aria-haspopup="true"><a href="login.html">Login</a></li>
                            <li aria-haspopup="true"><a href="register.html">Register</a></li>
                            <li aria-haspopup="true"><a href="forgot-password.html">Forgot Password</a></li>
                            <li aria-haspopup="true"><a href="lockscreen.html">Lock screen</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <!--Nav-->
        </div>
    </div>
</div>
<!--Horizontal-main -->