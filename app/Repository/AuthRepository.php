<?php

namespace App\Repository;

use App\Models\User;

class AuthRepository
{
    public function findByEmailOrUserName($data)
    {
        return User::where('email', $data)->orWhere('name', $data)->first();
    }
}