<?php

namespace App\Http\Controllers;

use App\Service\AuthService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $authService;

    public function __construct()
    {
        $this->authService = new AuthService;
    }

    public function login()
    {
        return view('backend.auth.login');
    }

    public function loginPost(Request $request)
    {
        $result = $this->authService->loginPost($request->all());
        alertNotify($result['status'], $result['message'], $request);
        if (!$result['status']) {
            return redirect()->back();
        }

        return redirect(url('/'));
    }

    public function logOut(Request $request)
    {
        $result = $this->authService->logOut();
        alertNotify($result['status'], $result['message'], $request);
        if (!$result['status']) {
            return redirect()->back();
        }
        return redirect(url('login'));
    }

}
