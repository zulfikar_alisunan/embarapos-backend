<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function productCategories()
    {
        return $this->belongsTo(ProductCategories::class);
    }

    public function productOrders()
    {
        return $this->hasMany(ProductOrders::class);
    }

    public function saleDetails()
    {
        return $this->hasMany(SaleDetails::class);
    }

    public function promo()
    {
        return $this->hasMany(Promo::class);
    }
}
