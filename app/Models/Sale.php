<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;
    protected $table = 'sale';
    protected $guarded = [];

    public function saleDetails()
    {
        return $this->hasMany(SaleDetails::class);
    }

    public function kas()
    {
        return $this->hasOne(Kas::class);
    }
}
