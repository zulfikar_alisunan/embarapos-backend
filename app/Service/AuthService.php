<?php

namespace App\Service;

use App\Repository\AuthRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthService {
    protected $authRepo;

    public function __construct()
    {
        $this->authRepo = new AuthRepository;
    }

    public function loginPost($data)
    {
        try {
            $user = $this->authRepo->findByEmailOrUserName($data['email_or_user_name']);
            if (!$user) {
                return returnCustom("Email atau username yang anda masukan salah");
            }

            if (!Hash::check($data['password'], $user->password)) {
                return returnCustom("Password yang anda masukan salah");
            }
            Auth::login($user);
            return returnCustom("Selamat datang " .$user->name, true);
        } catch (\Exception $e) {
            return returnCustom($e->getMessage());
        }
    }

    public function logOut()
    {
        try {
            if (!Auth::check()) {
                return returnCustom('Unknown user!');
            }
            Auth::logout();
            return returnCustom("Anda berhasil keluar", true);
        } catch (\Exception $e) {
            return returnCustom($e->getMessage());
        }
    }
}